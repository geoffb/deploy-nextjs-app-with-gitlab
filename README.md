## Demo Next.js application

This is a demo [Next.js](https://nextjs.org/) project to be deployed to Gitlab Pages.<br>
It has been bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Preview

![demo application preview](public/demo-application.gif)

## Credits

- The favicon is a free image: https://icons8.com/icon/46545/flower
- The image on the home page is also free: https://pixabay.com/photos/tulips-flowers-petals-blooms-8544741/
