import Button from '@/components/Button'

const About: React.FC = () => (
  <>
    <h1 className='text-5xl mb-10'>About page</h1>
    <Button text='< To home page' to='/' />
    <p>This is a demo application to be deployed to Gitlab Pages.</p>
  </>
)

export default About
