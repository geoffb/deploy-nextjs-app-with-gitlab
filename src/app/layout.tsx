import type { Metadata } from 'next'
import { Roboto } from 'next/font/google'
import './globals.css'

const roboto = Roboto({
  weight: '400',
  subsets: ['latin'],
  display: 'swap',
})

export const metadata: Metadata = {
  title: 'My beautiful Next.js application',
  description: 'How to deploy Next.js app to Gitlab pages',
  icons: {
    icon: '/icons8-flower-64.png',
  },
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <html lang='en'>
      <body className={roboto.className}>
        <main className='flex min-h-screen flex-col items-center justify-center p-24 bg-gradient-to-r from-yellow-200 via-green-200 to-green-300'>
          {children}
        </main>
      </body>
    </html>
  )
}
