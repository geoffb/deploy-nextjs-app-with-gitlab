import Image from 'next/image'
import Button from '@/components/Button'

const Home: React.FC = () => (
  <>
    <h1 className='text-5xl mb-10'>Home page</h1>
    <Button text='To about page >' to='/about' />
    <Image
      src='/tulips-8544741_640.jpg'
      width={640}
      height={427}
      alt='Yellow tulips'
    />
  </>
)

export default Home
