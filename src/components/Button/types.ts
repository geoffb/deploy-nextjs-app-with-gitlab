export type ButtonProps = {
  text: string
  to: string
}
