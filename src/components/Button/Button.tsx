import Link from 'next/link'
import { Button as Btn } from '@/components/ui/button'
import { ButtonProps } from './types'

const Button: React.FC<ButtonProps> = ({ text, to }) => (
  <Btn asChild size='lg' className='mb-10'>
    <Link href={to}>{text}</Link>
  </Btn>
)

export default Button
