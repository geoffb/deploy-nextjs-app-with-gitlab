/** @type {import("prettier").Config} */
const config = {
  jsxSingleQuote: true,
  semi: false,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: "es5"
}

export default config;